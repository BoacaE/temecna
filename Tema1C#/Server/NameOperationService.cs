﻿using Generated;
using Grpc.Core;
using System.Threading.Tasks;

namespace Server
{
    internal class NameOperationService : Generated.NameProcessing.NameProcessingBase
    {
        public override Task<NameResponse> Calculate(NameRequest request, ServerCallContext context)
    {
        var result = request.ClientName;

        System.Console.WriteLine("Mesaj primit ☺{0}☻ ", result);

        return Task.FromResult(new NameResponse() { });
    }
}
}
