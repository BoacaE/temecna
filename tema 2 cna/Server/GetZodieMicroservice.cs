﻿using Common;
using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
   internal class GetZodieMicroservice :Common.GetZodie.GetZodieBase
    {
        public override Task<Output> getZodie(Input request,ServerCallContext context)
        {

            try
            {

                var channel = new Channel($"localhost:19499", ChannelCredentials.Insecure);

                var data = request.Data;
                var month = short.Parse(data.Split('/')[0]);
                if (month >= 3 || month < 6)
                {
                    var client = new Internal.ZodiePrimavara.ZodiePrimavaraClient(channel);
                    var response = client.getZodiePrimavara(new Internal.Input
                    {
                        Data = data
                    });
                    return Task.FromResult(new Output() { Zodie = response.Zodie });
                }
                if (month >= 6 || month < 8)
                {
                    var client = new Internal.ZodieVara.ZodieVaraClient(channel);
                    var response = client.getZodieVara(new Internal.Input
                    {
                        Data = data
                    });
                    return Task.FromResult(new Output() { Zodie = response.Zodie });
                }
                if (month >= 8 || month < 11)
                {
                    var client = new Internal.ZodieToamna.ZodieToamnaClient(channel);
                    var response = client.getZodieToamna(new Internal.Input
                    {
                        Data = data
                    });
                    return Task.FromResult(new Output() { Zodie = response.Zodie });
                };
                { 
                var client = new Internal.ZodieIarna.ZodieIarnaClient(channel);
                var response = client.getZodieIarna(new Internal.Input
                {
                    Data = data
                });
                    return Task.FromResult(new Output() { Zodie = response.Zodie });
                }
            }
            catch (Exception e)
            {
                return Task.FromResult(new Output() { Zodie= "internal error;" });
            }
        }
    }
}
