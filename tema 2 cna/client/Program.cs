﻿using Grpc.Core;
using System;
using System.Threading;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {

            const int Port = 88888;

            var channel = new Channel($"localhost:{Port}", ChannelCredentials.Insecure);

            Console.WriteLine("Introduceti o data in forma ll/zz/aaaa:");

            do
            {
                var input = Console.ReadLine();
                var errormsg = validate_input(input);
                if(errormsg == string.Empty)
                {
                    var client = new Common.GetZodie.GetZodieClient(channel);
                    try
                    {
                        var response = client.getZodie(new Common.Input
                        {
                            Data = input
                        });
                        System.Console.WriteLine(response.Zodie);

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
                else
                {
                    Console.WriteLine(errormsg);
                }




            } while (true);


            // Shutdown
            channel.ShutdownAsync().Wait();
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

        private static string validate_input(string input)
        {
            
            short luna, zi, an;
            var split = input.Split('/');
            if (split.Length != 3)
                return "format invalid";
            if (!Int16.TryParse(split[0], out luna))
            {
                return "format invalid";
            }
            if (!Int16.TryParse(split[1], out zi))
            {
                return "format invalid";
            }
            if (!Int16.TryParse(split[1], out an))
            {
                return "format invalid";
            }
            if (luna < 1 || luna > 12)
                return "data invalida";
            if (zi < 1 || zi > 31)
            {
                return "data invalida";
            }
            if (luna == 4 || luna == 6 || luna == 9 || luna == 11)
                if (zi == 31)
                    return "data invalida";
            if (luna == 2)
            {
                if (zi > 29)
                    return "data invalida";
                if (an % 4 != 0)
                {
                    if (zi == 29)
                        return "data invalida";
                }
                else if (an % 100 == 0)
                {
                    if (zi == 29)
                        return "data invalida";
                }
                else if (an % 400 != 0)
                {
                    if (zi == 29)
                        return "data invalida";
                }
            }
            return string.Empty;
        }
    }
}
