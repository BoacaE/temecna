// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: GetZodie.proto
// </auto-generated>
#pragma warning disable 1591, 0612, 3021
#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;
namespace Common {

  /// <summary>Holder for reflection information generated from GetZodie.proto</summary>
  public static partial class GetZodieReflection {

    #region Descriptor
    /// <summary>File descriptor for GetZodie.proto</summary>
    public static pbr::FileDescriptor Descriptor {
      get { return descriptor; }
    }
    private static pbr::FileDescriptor descriptor;

    static GetZodieReflection() {
      byte[] descriptorData = global::System.Convert.FromBase64String(
          string.Concat(
            "Cg5HZXRab2RpZS5wcm90bxoLSW5wdXQucHJvdG8aDE91dHB1dC5wcm90bzIn",
            "CghHZXRab2RpZRIbCghnZXRab2RpZRIGLklucHV0GgcuT3V0cHV0QgmqAgZD",
            "b21tb25iBnByb3RvMw=="));
      descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
          new pbr::FileDescriptor[] { global::Common.InputReflection.Descriptor, global::Common.OutputReflection.Descriptor, },
          new pbr::GeneratedClrTypeInfo(null, null, null));
    }
    #endregion

  }
}

#endregion Designer generated code
