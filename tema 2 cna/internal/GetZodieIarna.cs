// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: GetZodieIarna.proto
// </auto-generated>
#pragma warning disable 1591, 0612, 3021
#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;
namespace Internal {

  /// <summary>Holder for reflection information generated from GetZodieIarna.proto</summary>
  public static partial class GetZodieIarnaReflection {

    #region Descriptor
    /// <summary>File descriptor for GetZodieIarna.proto</summary>
    public static pbr::FileDescriptor Descriptor {
      get { return descriptor; }
    }
    private static pbr::FileDescriptor descriptor;

    static GetZodieIarnaReflection() {
      byte[] descriptorData = global::System.Convert.FromBase64String(
          string.Concat(
            "ChNHZXRab2RpZUlhcm5hLnByb3RvGgtJbnB1dC5wcm90bxoMT3V0cHV0LnBy",
            "b3RvMi4KClpvZGllSWFybmESIAoNZ2V0Wm9kaWVJYXJuYRIGLklucHV0Ggcu",
            "T3V0cHV0QguqAghJbnRlcm5hbGIGcHJvdG8z"));
      descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
          new pbr::FileDescriptor[] { global::Internal.InputReflection.Descriptor, global::Internal.OutputReflection.Descriptor, },
          new pbr::GeneratedClrTypeInfo(null, null, null));
    }
    #endregion

  }
}

#endregion Designer generated code
