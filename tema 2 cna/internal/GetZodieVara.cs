// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: GetZodieVara.proto
// </auto-generated>
#pragma warning disable 1591, 0612, 3021
#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;
namespace Internal {

  /// <summary>Holder for reflection information generated from GetZodieVara.proto</summary>
  public static partial class GetZodieVaraReflection {

    #region Descriptor
    /// <summary>File descriptor for GetZodieVara.proto</summary>
    public static pbr::FileDescriptor Descriptor {
      get { return descriptor; }
    }
    private static pbr::FileDescriptor descriptor;

    static GetZodieVaraReflection() {
      byte[] descriptorData = global::System.Convert.FromBase64String(
          string.Concat(
            "ChJHZXRab2RpZVZhcmEucHJvdG8aC0lucHV0LnByb3RvGgxPdXRwdXQucHJv",
            "dG8yLAoJWm9kaWVWYXJhEh8KDGdldFpvZGllVmFyYRIGLklucHV0GgcuT3V0",
            "cHV0QguqAghJbnRlcm5hbGIGcHJvdG8z"));
      descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
          new pbr::FileDescriptor[] { global::Internal.InputReflection.Descriptor, global::Internal.OutputReflection.Descriptor, },
          new pbr::GeneratedClrTypeInfo(null, null, null));
    }
    #endregion

  }
}

#endregion Designer generated code
