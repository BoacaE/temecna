// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: SetZodie.proto
// </auto-generated>
#pragma warning disable 0414, 1591
#region Designer generated code

using grpc = global::Grpc.Core;

namespace Internal {
  public static partial class SetZodie
  {
    static readonly string __ServiceName = "SetZodie";

    static readonly grpc::Marshaller<global::Internal.InputSet> __Marshaller_InputSet = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Internal.InputSet.Parser.ParseFrom);
    static readonly grpc::Marshaller<global::Internal.OutputSet> __Marshaller_OutputSet = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Internal.OutputSet.Parser.ParseFrom);

    static readonly grpc::Method<global::Internal.InputSet, global::Internal.OutputSet> __Method_set_Zodie = new grpc::Method<global::Internal.InputSet, global::Internal.OutputSet>(
        grpc::MethodType.Unary,
        __ServiceName,
        "set_Zodie",
        __Marshaller_InputSet,
        __Marshaller_OutputSet);

    /// <summary>Service descriptor</summary>
    public static global::Google.Protobuf.Reflection.ServiceDescriptor Descriptor
    {
      get { return global::Internal.SetZodieReflection.Descriptor.Services[0]; }
    }

    /// <summary>Base class for server-side implementations of SetZodie</summary>
    [grpc::BindServiceMethod(typeof(SetZodie), "BindService")]
    public abstract partial class SetZodieBase
    {
      public virtual global::System.Threading.Tasks.Task<global::Internal.OutputSet> set_Zodie(global::Internal.InputSet request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

    }

    /// <summary>Client for SetZodie</summary>
    public partial class SetZodieClient : grpc::ClientBase<SetZodieClient>
    {
      /// <summary>Creates a new client for SetZodie</summary>
      /// <param name="channel">The channel to use to make remote calls.</param>
      public SetZodieClient(grpc::ChannelBase channel) : base(channel)
      {
      }
      /// <summary>Creates a new client for SetZodie that uses a custom <c>CallInvoker</c>.</summary>
      /// <param name="callInvoker">The callInvoker to use to make remote calls.</param>
      public SetZodieClient(grpc::CallInvoker callInvoker) : base(callInvoker)
      {
      }
      /// <summary>Protected parameterless constructor to allow creation of test doubles.</summary>
      protected SetZodieClient() : base()
      {
      }
      /// <summary>Protected constructor to allow creation of configured clients.</summary>
      /// <param name="configuration">The client configuration.</param>
      protected SetZodieClient(ClientBaseConfiguration configuration) : base(configuration)
      {
      }

      public virtual global::Internal.OutputSet set_Zodie(global::Internal.InputSet request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return set_Zodie(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual global::Internal.OutputSet set_Zodie(global::Internal.InputSet request, grpc::CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_set_Zodie, null, options, request);
      }
      public virtual grpc::AsyncUnaryCall<global::Internal.OutputSet> set_ZodieAsync(global::Internal.InputSet request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return set_ZodieAsync(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual grpc::AsyncUnaryCall<global::Internal.OutputSet> set_ZodieAsync(global::Internal.InputSet request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_set_Zodie, null, options, request);
      }
      /// <summary>Creates a new instance of client from given <c>ClientBaseConfiguration</c>.</summary>
      protected override SetZodieClient NewInstance(ClientBaseConfiguration configuration)
      {
        return new SetZodieClient(configuration);
      }
    }

    /// <summary>Creates service definition that can be registered with a server</summary>
    /// <param name="serviceImpl">An object implementing the server-side handling logic.</param>
    public static grpc::ServerServiceDefinition BindService(SetZodieBase serviceImpl)
    {
      return grpc::ServerServiceDefinition.CreateBuilder()
          .AddMethod(__Method_set_Zodie, serviceImpl.set_Zodie).Build();
    }

    /// <summary>Register service method with a service binder with or without implementation. Useful when customizing the  service binding logic.
    /// Note: this method is part of an experimental API that can change or be removed without any prior notice.</summary>
    /// <param name="serviceBinder">Service methods will be bound by calling <c>AddMethod</c> on this object.</param>
    /// <param name="serviceImpl">An object implementing the server-side handling logic.</param>
    public static void BindService(grpc::ServiceBinderBase serviceBinder, SetZodieBase serviceImpl)
    {
      serviceBinder.AddMethod(__Method_set_Zodie, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::Internal.InputSet, global::Internal.OutputSet>(serviceImpl.set_Zodie));
    }

  }
}
#endregion
