﻿namespace microserver
{
    class Configuration
    {
        public const string HOST = "0.0.0.0";
        public const int PORT = 19499;
    }
}
