﻿using Grpc.Core;
using Internal;
using microserver_2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace microserver
{
    class PrimavaraMicroservice:Internal.ZodiePrimavara.ZodiePrimavaraBase
    {
        public override Task<Output> getZodiePrimavara(Input input, ServerCallContext context)
        {
            String data = input.Data;
            string luna, zi, an;
            var split = data.Split('/');
            luna = split[0]; zi = split[1]; an = split[2];


            return Task.FromResult(new Output() { Zodie = Zodii.getzodie(int.Parse(zi), int.Parse(luna)) });



        }
    }
}
