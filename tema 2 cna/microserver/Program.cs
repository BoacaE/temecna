﻿using Grpc.Core;
using microserver_2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace microserver
{
    class Program
    {
        static void Main(string[] args)
        {
            Zodii.load("./zodii.txt");
            using( Server server = new Server(Configuration.HOST, Configuration.PORT))
            {
                server.CloseServerAction = () => Console.ReadKey();
                server.Start();
            }
        }
    }
}
