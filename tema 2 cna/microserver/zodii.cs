﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace microserver_2
{
    class Zodii
    {
        public struct zodii
        {
            public string startdate;
            public string enddate;
            public string zodie;
        }
       public static List<zodii> zodiis=null;
public static void load(string path)
        {
            try
            {
                var read = System.IO.File.ReadAllLines(path);
                zodiis = new List<zodii>();
              
                foreach (var curent in read)
                {
                    var split = curent.Split(' ');
                    zodii z = new zodii();
                    z.zodie = split[0];
                    z.startdate = split[1];
                    z.enddate = split[2];
                    zodiis.Add(z);
                }
            }catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
public static string getzodie(int zi,int luna)
        {
            if(zodiis==null)
                load("./zodii.txt");
            if (zodiis != null)
                foreach (zodii z in zodiis)
            {
                var splits=z.startdate.Split('/');
                if (int.Parse(splits[0]) == luna)
                    if (int.Parse(splits[1]) <= zi)
                        return z.zodie;
                 splits = z.enddate.Split('/');
                if (int.Parse(splits[0]) == luna)
                    if (int.Parse(splits[1]) >= zi)
                        return z.zodie;
            }
            return "internal error";
        }

    }
}
